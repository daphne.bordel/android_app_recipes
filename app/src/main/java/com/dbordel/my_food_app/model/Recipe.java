package com.dbordel.my_food_app.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity
(tableName = "recette",foreignKeys =
@ForeignKey(entity = Ingredient.class,
        parentColumns = "id_ing",
        childColumns = "IngredientId",
        onDelete = CASCADE))
public class Recipe implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    private int id_recette;
    @ColumnInfo(name = "IngredientId")
    public int IngredientId;
    @ColumnInfo(name = "nomRecette")
    private String nom;
    @ColumnInfo(name = "preparation")
    private String preparation;


    public Recipe (int id, String nom, String preparation){
        this.id_recette = id;
        this.nom = nom;
        this.preparation = preparation;
    }

    public Recipe() {
    }

    protected Recipe(Parcel in) {
        id_recette = in.readInt();
        nom = in.readString();
        preparation = in.readString();
        IngredientId = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Recipe> CREATOR = new Creator<Recipe>() {
        @Override
        public Recipe createFromParcel(Parcel in) {
            return new Recipe(in);
        }

        @Override
        public Recipe[] newArray(int size) {
            return new Recipe[size];
        }
    };

    public int getId_recette() {
        return id_recette;
    }

    public void setId_recette(int id_recette) {
        this.id_recette = id_recette;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPreparation(){return preparation;}

    public void setPreparation(String preparation){this.preparation = preparation;}

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id_recette);
        dest.writeString(nom);
        dest.writeString(preparation);
        dest.writeInt(IngredientId);
    }

    @Override
    public String toString() {
        return nom  ;
    }
}









