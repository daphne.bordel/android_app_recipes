package  com.dbordel.my_food_app.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;


@Entity (tableName = "Ingredient")
public class Ingredient implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    public int id_ing;
    @ColumnInfo(name = "nomIngredient")
    public String nom;

    public Ingredient() {
    }

    protected Ingredient(Parcel in) {
        id_ing = in.readInt();
        nom = in.readString();
    }

    public static final Creator<Ingredient> CREATOR = new Creator<Ingredient>() {
        @Override
        public Ingredient createFromParcel(Parcel in) {
            return new Ingredient(in);
        }

        @Override
        public Ingredient[] newArray(int size) {
            return new Ingredient[size];
        }
    };

    public int getId_ing() {
        return id_ing;
    }

    public String getNom() {
        return nom;
    }

    public void setId_ing(int id_ing) {
        this.id_ing = id_ing;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return nom ;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id_ing);
        dest.writeString(nom);
    }

}
