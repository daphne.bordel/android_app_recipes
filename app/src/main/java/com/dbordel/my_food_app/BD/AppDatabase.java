package com.dbordel.my_food_app.BD;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.dbordel.my_food_app.model.Ingredient;
import com.dbordel.my_food_app.model.Recipe;

@Database(entities = {Ingredient.class, Recipe.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE = null;

    public abstract IngredientDAO getIngredientDAO();
    public abstract RecipeDAO getRecipeDAO();

    public static synchronized AppDatabase getDatabaseInstance(Context context){
        if (INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class,"kitchen_database").allowMainThreadQueries().build();
        }
        return INSTANCE;
    }

}