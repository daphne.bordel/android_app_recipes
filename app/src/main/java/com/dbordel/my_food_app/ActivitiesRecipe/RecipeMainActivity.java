package com.dbordel.my_food_app.ActivitiesRecipe;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import com.dbordel.my_food_app.ActivitiesIngredient.IngredientMainActivity;
import com.dbordel.my_food_app.ActivitiesIngredient.IngredientsListActivity;
import com.dbordel.my_food_app.BD.AppDatabase;
import com.dbordel.my_food_app.BD.IngredientDAO;
import com.dbordel.my_food_app.BD.RecipeDAO;
import com.dbordel.my_food_app.model.Ingredient;
import com.dbordel.my_food_app.model.Recipe;
import com.dbordel.my_food_app.R;

import java.util.List;

public class RecipeMainActivity extends AppCompatActivity {

    private static final int REQUEST_IMAGE_CAPTURE = 1 ;
    private EditText nom, preparation;

    RecipeDAO recipeDAO;

    List<Ingredient> Ingrecette;

    Integer IngrIdPosition;

    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recette_activity_main);

        nom = findViewById(R.id.editTextnomRecette);
        preparation = findViewById(R.id.editTextPreparationRecette);
        spinner = findViewById(R.id.spinnerIngredient);

        Ingrecette = AppDatabase.getDatabaseInstance(this).getIngredientDAO().getIngredients();

        ArrayAdapter<Ingredient> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, Ingrecette);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                IngrIdPosition = 1 + pos;
                Log.d("ingredientslogs","position :" + pos + "RecIdPosition :" + IngrIdPosition );
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

       recipeDAO = AppDatabase.getDatabaseInstance(this).getRecipeDAO();

    }

//-------------------------------  SAUVEGARDER Recette  -------------------------------------

    //sauvegarder les informations de la recette

    public void saveRecette(View view) {
        Log.d("enter in saveRecette","save recette");
        if(nom.getText().toString().isEmpty() || preparation.getText().toString().isEmpty()){
            Log.d("I am in the if","enter");
            Toast.makeText(this, "Il manque des infos", Toast.LENGTH_SHORT).show();
        }else{
            Log.d("I am in the else",nom.getText().toString());
            Log.d("preparation",preparation.getText().toString());
            Log.d("id", String.valueOf(IngrIdPosition.intValue()));
            Recipe recipe = new Recipe();
            recipe.setNom(nom.getText().toString());
            recipe.setPreparation(preparation.getText().toString());
            recipe.setId_recette(IngrIdPosition.intValue());
            Log.d("recipe before save",recipe.toString());
            recipeDAO.insert(recipe);

            Toast.makeText(this, "Recette ajoutée !", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, RecipeMainActivity.class);
            startActivity(intent);

        }
    }

    //---------------------------------- AFFICHER LES RecetteS ---------------------------------------

    //afficher la liste des recettes créés

    public void showListeRecette(View view){
        Intent intent = new Intent(this, RecipeListActivity.class);
        startActivity(intent);
    }

    //--------------------------------- AFFICHER LES INGREDIENTS ---------------------------------------
    //afficher la liste des ingrédients
    public void showAllIngredients(View view){
        Intent intent = new Intent(this, IngredientsListActivity.class);
        startActivity(intent);
    }

    //--------------------------------- AJOUTER DES INGREDIENTS ---------------------------------------
    //ajouter des ingrédients
    public void addIngredient(View view){
        Intent intent = new Intent(this, IngredientMainActivity.class);
        startActivity(intent);
    }
}
