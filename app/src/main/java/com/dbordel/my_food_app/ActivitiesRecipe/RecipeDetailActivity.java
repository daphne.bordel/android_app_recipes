package com.dbordel.my_food_app.ActivitiesRecipe;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.dbordel.my_food_app.R;
import com.dbordel.my_food_app.model.Recipe;

public class RecipeDetailActivity extends AppCompatActivity {

    TextView nom,preparation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recette_activity_detail);

        nom = findViewById(R.id.recetteNom);
        preparation = findViewById(R.id.recettePreparation);

        if(getIntent().hasExtra("recette_selected")){
            Recipe recipe = getIntent().getParcelableExtra("recette_selected");
            nom.setText(recipe.getNom());
            preparation.setText(recipe.getPreparation());

            Log.d("meslogs", "test infos recup " + recipe.toString());
        }
    }

    public void retourListeRecette(View v){ finish();}
}