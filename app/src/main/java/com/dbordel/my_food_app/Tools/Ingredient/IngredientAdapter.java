package com.dbordel.my_food_app.Tools.Ingredient;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dbordel.my_food_app.model.Ingredient;
import com.dbordel.my_food_app.R;
import com.dbordel.my_food_app.Tools.ItemClickListener;

import java.util.List;


public class IngredientAdapter extends RecyclerView.Adapter<IngredientViewHolder> implements View.OnClickListener {

    List<Ingredient> lesIngredients;
    private ItemClickListener mItemClickListener;

    public IngredientAdapter(List<Ingredient> listeIngredients, ItemClickListener itemClickListener){
        this.lesIngredients = listeIngredients;
        this.mItemClickListener = itemClickListener;
    }


    //Create new views for RecyclerView (invoked by the layoutManager)
    @NonNull
    @Override
    public IngredientViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int ViewType){
        // create a new view
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.ingredient_item_layout,parent,false);
        IngredientViewHolder ingredientViewHolder = new IngredientViewHolder(view,mItemClickListener);
        return ingredientViewHolder;
    }

    //Replace the contents of a view (invoked by the layoutManager)
    @Override
    public void onBindViewHolder(@NonNull final IngredientViewHolder ingredientViewHolder, final int position) {
        //get element from the dataset at this position
        //replace the contents of the view with that element
        final Ingredient ingredient = lesIngredients.get(position);
        ingredientViewHolder.nom.setText(ingredient.getNom());

    }

    @Override
    public int getItemCount() {
        return lesIngredients.size();
    }

    @Override
    public void onClick(View v) {
    }

    //utile pour la suppression
    public Ingredient getLIngredient(int position){
            return  lesIngredients.get(position);

    }
}





