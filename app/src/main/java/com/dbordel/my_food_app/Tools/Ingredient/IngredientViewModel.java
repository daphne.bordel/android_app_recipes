package com.dbordel.my_food_app.Tools.Ingredient;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.dbordel.my_food_app.BD.AppDatabase;
import com.dbordel.my_food_app.BD.IngredientDAO;
import com.dbordel.my_food_app.model.Ingredient;
import com.dbordel.my_food_app.Repository.IngredientRepository;

import java.util.List;


public class IngredientViewModel extends AndroidViewModel {
    private IngredientRepository ingredientRepository;
    private AppDatabase database;
    IngredientDAO ingredientDao;
    LiveData<Integer> nbIngredientsLD;

    public IngredientViewModel(@NonNull Application application) {
        super(application);
        ingredientRepository = new IngredientRepository(application);
        database = AppDatabase.getDatabaseInstance(application);
        ingredientDao = database.getIngredientDAO();
        nbIngredientsLD = ingredientRepository.getNbIngredientsLD();
    }

    public void update(Ingredient ingredient){
        ingredientRepository.update(ingredient);
    }

    public void delete(Ingredient ingredient){
        ingredientRepository.delete(ingredient);
    }

    public LiveData<List<Ingredient>> getIngredientsLD(){ return ingredientRepository.getIngredientLD();
    }

    public LiveData<Integer> getNbIngredientsLD() {
        return nbIngredientsLD;
    }


}


