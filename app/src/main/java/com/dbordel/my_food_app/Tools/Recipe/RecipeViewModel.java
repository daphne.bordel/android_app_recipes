package com.dbordel.my_food_app.Tools.Recipe;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.dbordel.my_food_app.Repository.RecipeRepository;
import com.dbordel.my_food_app.model.Recipe;

import java.util.List;


public class RecipeViewModel extends AndroidViewModel {
    private RecipeRepository recipeRepository;

    public RecipeViewModel(@NonNull Application application) {
        super(application);
        recipeRepository = new RecipeRepository(application);
    }

    public LiveData<List<Recipe>> getRecetteLD(){
        return recipeRepository.getRecipeLD();
    }

    public List<Recipe> getRecipes() {
        return recipeRepository.getRecipes();
    }

    public void insert(Recipe recipe){
         recipeRepository.insert(recipe);
    }

    public void update(Recipe recipe){
        recipeRepository.update(recipe);
    }

    public void delete(Recipe recette){
        recipeRepository.delete(recette);
    }

    public void deleteAll(){
        recipeRepository.deleteAll();
    }


}



