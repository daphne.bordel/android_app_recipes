package com.dbordel.my_food_app.Tools;

public interface ItemClickListener {

    void onShowClick(int position);

    void onUpdateClick(int position);

    void onDeleteClick(int position);



}
