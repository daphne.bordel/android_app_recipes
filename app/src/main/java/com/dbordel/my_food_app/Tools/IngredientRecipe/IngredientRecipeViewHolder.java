package com.dbordel.my_food_app.Tools.IngredientRecipe;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.my_food_app.R;

public class IngredientRecipeViewHolder extends RecyclerView.ViewHolder{
    TextView nom;

    public IngredientRecipeViewHolder(@NonNull View itemView) {
        super(itemView);
        nom = itemView.findViewById(R.id.cardViewNomRecette);
    }
}
