package com.dbordel.my_food_app.Tools.Recipe;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.my_food_app.R;
import com.dbordel.my_food_app.Tools.ItemClickListener;
import com.dbordel.my_food_app.model.Recipe;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;


//Un ViewHolder permet de placer les objets dans les cellules de la RecyclerView
public class RecipeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private List<Recipe> recipes;
    TextView nom;
    FloatingActionButton boutonShow,boutonDelete,boutonEdit;


    ItemClickListener itemClickListener;

    public RecipeViewHolder(@NonNull View itemView, ItemClickListener itemClickListener) {
        super(itemView);
        nom = itemView.findViewById(R.id.cardViewnomRecette);
        boutonShow = itemView.findViewById(R.id.buttonShowRecette);
        boutonDelete = itemView.findViewById(R.id.buttonDeleteRecette);
        boutonEdit = itemView.findViewById(R.id.buttonUpdateRecette);

        this.itemClickListener = itemClickListener;
        //itemView.setOnClickListener(this);
        boutonDelete.setOnClickListener(this);
        boutonEdit.setOnClickListener(this);
        boutonShow.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.buttonShowRecette:
                itemClickListener.onShowClick(getAdapterPosition());
                break;

            case R.id.buttonUpdateRecette:
                itemClickListener.onUpdateClick(getAdapterPosition());
                break;

            case R.id.buttonDeleteRecette:
                itemClickListener.onDeleteClick(getAdapterPosition());
                break;

            default:
                break;
        }
    }

}


