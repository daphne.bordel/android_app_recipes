package com.dbordel.my_food_app.ActivitiesSuggestionsRecipe;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;

import com.dbordel.my_food_app.Network.HttpGetRequest;
import com.dbordel.my_food_app.R;
import com.dbordel.my_food_app.model.ApiRecipe;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class ApiSearchRecipesActivity extends AppCompatActivity {
    JSONObject result = null;
    List<ApiRecipe> data = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //show the current layout for this activity
        setContentView(R.layout.search_api_recipes_activity);

        //if user search recipe
        SearchView simpleSearchView = (SearchView) findViewById(R.id.search_recipe_view);
        simpleSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String myUrl = createURL(query);
                String result;
                HttpGetRequest myRequest = new HttpGetRequest();

                try {
                    GsonBuilder gsonb = new GsonBuilder();
                    Gson gson = gsonb.create();
                    result = myRequest.execute(myUrl).get();
                    JSONObject obj = new JSONObject(result);

                    //start new activity
                    Intent searchResultIntent = new Intent(ApiSearchRecipesActivity.this,ApiSearchResultRecipesActivity.class);
                    searchResultIntent.putExtra("data",obj.toString());
                    startActivity(searchResultIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }
    private String createURL(String query) {
        String baseUrl = "https://recipesapi.herokuapp.com/api/search?q=";
        try {
            String urlString = baseUrl + URLEncoder.encode(query, "UTF-8");
            return urlString;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void onClickEntreeBlock(View view) {
        Intent recipesListActivityIntent = new Intent(ApiSearchRecipesActivity.this, ApiRecipesListActivity.class);
        Log.d("on clique sur:","entree");
        recipesListActivityIntent.putExtra("query","salad");
        startActivity(recipesListActivityIntent);
    }

    public void onClickBeefBlock(View view) {
        Intent recipesListActivityIntent = new Intent(ApiSearchRecipesActivity.this, ApiRecipesListActivity.class);
        recipesListActivityIntent.putExtra("query","beef");
        startActivity(recipesListActivityIntent);
    }

    public void onClickCakeBlock(View view) {
        Intent recipesListActivityIntent = new Intent(ApiSearchRecipesActivity.this, ApiRecipesListActivity.class);
        recipesListActivityIntent.putExtra("query","cake");
        startActivity(recipesListActivityIntent);
    }

    public void onClickChineseBlock(View view){
        Intent recipesListActivityIntent = new Intent(ApiSearchRecipesActivity.this, ApiRecipesListActivity.class);
        recipesListActivityIntent.putExtra("query","chinese");
        startActivity(recipesListActivityIntent);
    }

    public void onClickCrepesBlock(View view){
        Intent recipesListActivityIntent = new Intent(ApiSearchRecipesActivity.this, ApiRecipesListActivity.class);
        recipesListActivityIntent.putExtra("query","crepes");
        startActivity(recipesListActivityIntent);
    }
}