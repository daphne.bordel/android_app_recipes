package com.dbordel.my_food_app.ActivitiesSuggestionsRecipe;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.my_food_app.Network.HttpGetRequest;
import com.dbordel.my_food_app.R;
import com.dbordel.my_food_app.Tools.ApiRecipe.ApiRecipeAdapter;
import com.dbordel.my_food_app.model.ApiRecipe;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class ApiRecipesListActivity  extends AppCompatActivity {
    JSONArray mRecipesList;
    List<ApiRecipe> mRecipes = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private ApiRecipeAdapter mAdapter;
    private String query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_result_api_recipe_activity);
        Intent intent = getIntent();
        query = intent.getStringExtra("query");
        //show all recipes with chicken
        Log.d("query ?",query);
        String myUrl = createURL(query);
        String result;
        HttpGetRequest myRequest = new HttpGetRequest();
        JSONObject obj = null;

        try {
            GsonBuilder gsonb = new GsonBuilder();
            Gson gson = gsonb.create();
            result = myRequest.execute(myUrl).get();
            obj = new JSONObject(result);
            mRecipesList = obj.getJSONArray("recipes");
            for (int i=0; i< mRecipesList.length(); i++){
                try {
                    JSONObject recipe = (JSONObject) mRecipesList.get(i);
                    ApiRecipe myRecipe = new ApiRecipe(
                            recipe.getString("title"),
                            recipe.getString("image_url"),
                            recipe.getString("publisher"),
                            recipe.getString("source_url"),
                            recipe.getInt("social_rank")
                    );
                    Log.d("ApiRecipe",myRecipe.getTitle());
                    mRecipes.add(myRecipe);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //recycler view
        if (mRecipes.size()>0){
            mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_items_search);
            // Create adapter passing in the sample recipe data
            mAdapter = new ApiRecipeAdapter(mRecipes,this);
            // Attach the adapter to the recyclerview to populate items
            mRecyclerView.setAdapter(mAdapter);
            // Set layout manager to position the items
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
    }

    private String createURL(String query) {
        String baseUrl = "https://recipesapi.herokuapp.com/api/search?q=";
        try {
            String urlString = baseUrl + URLEncoder.encode(query, "UTF-8");
            return urlString;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}