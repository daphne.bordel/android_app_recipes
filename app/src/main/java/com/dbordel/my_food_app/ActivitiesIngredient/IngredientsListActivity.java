package com.dbordel.my_food_app.ActivitiesIngredient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.dbordel.my_food_app.ActivitiesRecipe.RecipeMainActivity;
import com.dbordel.my_food_app.BD.AppDatabase;
import com.dbordel.my_food_app.BD.IngredientDAO;

import com.dbordel.my_food_app.MainActivity;
import com.dbordel.my_food_app.R;
import com.dbordel.my_food_app.Tools.ItemClickListener;
import com.dbordel.my_food_app.Tools.Ingredient.IngredientAdapter;
import com.dbordel.my_food_app.Tools.Ingredient.IngredientViewModel;



public class IngredientsListActivity extends AppCompatActivity implements ItemClickListener {

    RecyclerView recyclerView;
    IngredientDAO ingredientDAO;
    IngredientViewModel ingredientViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ingredient_activity_liste);

        recyclerView = findViewById(R.id.IngredientRecyclerView);
        ingredientDAO = AppDatabase.getDatabaseInstance(this).getIngredientDAO();
        IngredientAdapter ingredientAdapter = new IngredientAdapter(ingredientDAO.getIngredients(),this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(ingredientAdapter);
    }


//-------------------------------------------------------------------------------------------

    @Override
    public void onShowClick(int position) {
        Log.d("meslogs", "test bouton cliqué show  " +position);
        Intent intent = new Intent(this, IngredientDetailActivity.class);
        intent.putExtra("ingredient_selected", ingredientDAO.getIngredients().get(position));
        startActivity(intent);
    }
    @Override
    public void onUpdateClick(int position) {
        Log.d("meslogs", "test bouton cliqué update  "+ position);
        Intent intent = new Intent(this, IngredientUpdateActivity.class);
        intent.putExtra("updateIngredient", ingredientDAO.getIngredients().get(position));
        startActivity(intent);
    }

    @Override
    public void onDeleteClick(int position) {
        Log.d("meslogs", "test bouton cliqué delete  " + position );
        //ingredientDAO = AppDatabase.getDatabaseInstance(this).getIngredientDAO();
        IngredientAdapter ingredientAdapter = new IngredientAdapter(ingredientDAO.getIngredients(),this);
        ingredientDAO.deleteIngredient(ingredientAdapter.getLIngredient(position));
        recreate();
    }

    public void returnHome(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public static class MyRecipesActivity extends AppCompatActivity {
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.my_recipes_activity);
        }

        public void recetteLink(View view){
            Intent intent = new Intent(this, RecipeMainActivity.class);
            startActivity(intent);
        }

        public void ingredientLink(View view){
            Intent intent = new Intent(this, IngredientMainActivity.class);
            startActivity(intent);
        }
    }

}

