package com.dbordel.my_food_app.ActivitiesIngredient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.dbordel.my_food_app.BD.IngredientDAO;
import com.dbordel.my_food_app.BD.AppDatabase;
import com.dbordel.my_food_app.model.Ingredient;
import com.dbordel.my_food_app.R;
import com.dbordel.my_food_app.model.Recipe;

import java.util.List;

public class IngredientMainActivity extends AppCompatActivity {

    private EditText nom;

    Integer RecIdPosition;

    IngredientDAO ingredientDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ingredient_activity_main);

        nom = findViewById(R.id.editTextIngredientNom);

        ingredientDAO = AppDatabase.getDatabaseInstance(this).getIngredientDAO();
    }


    //-------------------------------  SAUVEGARDER INGREDIENT  -------------------------------------

    //sauvegarder les informations de l'ingredient

    public void SaveIngredient(View view) {
        if(nom.getText().toString().isEmpty()) {
            Toast.makeText(this, "Il manque des infos", Toast.LENGTH_SHORT).show();
        }else{
            Ingredient ingredient = new Ingredient();
            ingredient.setNom(nom.getText().toString());

            ingredientDAO.insertIngredient(ingredient);

            Toast.makeText(this, "Ingredient ajouté !", Toast.LENGTH_SHORT).show();
            recreate();
        }
    }

    //---------------------------------- AFFICHER LES INGREDIENTS ---------------------------------------

    public void ShowIngredients(View view) {
        Intent intent = new Intent(this, IngredientsListActivity.class);
        startActivity(intent);
    }



}
