package com.dbordel.my_food_app.ActivitiesIngredient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.dbordel.my_food_app.ActivitiesRecipe.RecipeListActivity;
import com.dbordel.my_food_app.BD.AppDatabase;
import com.dbordel.my_food_app.BD.IngredientDAO;
import com.dbordel.my_food_app.model.Ingredient;
import com.dbordel.my_food_app.R;
import com.dbordel.my_food_app.Tools.Ingredient.IngredientViewModel;
import com.dbordel.my_food_app.model.Recipe;

public class IngredientUpdateActivity extends AppCompatActivity {

    EditText nom;
    IngredientViewModel ingredientViewModel;
    IngredientDAO ingredientDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ingredient_activity_update);

        //ingredientViewModel = new ViewModelProvider(this).get(IngredientViewModel.class);

        nom = findViewById(R.id.editTextIngredientNomEdit);

        if(getIntent().hasExtra("updateIngredient")){
            Ingredient ingredient = getIntent().getParcelableExtra("updateIngredient");
            nom.setText(ingredient.getNom());
            Log.d("meslogs ","test données ingredient : " + ingredient.toString());
        }


        ingredientDAO = AppDatabase.getDatabaseInstance(this).getIngredientDAO();

    }

    public void updateIngredient(View view) {

        Ingredient ingredient = getIntent().getParcelableExtra("updateIngredient");
        ingredient.setNom(nom.getText().toString());

        ingredientDAO.updateIngredient(ingredient);
        Intent intent = new Intent(this, IngredientsListActivity.class);

        Log.d("meslogs", "update Ingredient :  " + ingredient.toString());

        Toast.makeText(this, "Ingredient modifié !", Toast.LENGTH_SHORT).show();
        startActivity(intent);
    }



    public void cancelUpdate(View view) {
        finish();
    }
}