package com.dbordel.my_food_app;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.dbordel.my_food_app.BD.AppDatabase;
import com.dbordel.my_food_app.BD.IngredientDAO;
import com.dbordel.my_food_app.BD.RecipeDAO;
import com.dbordel.my_food_app.Tools.Ingredient.IngredientViewModel;
import com.dbordel.my_food_app.Tools.IngredientRecipe.IngredientRecipeAdapter;
import com.dbordel.my_food_app.Tools.Recipe.RecipeViewModel;

public class IngredientRecipeListeActivity extends AppCompatActivity {
    RecipeViewModel recipeViewModel;
    RecyclerView recyclerView;
    RecipeDAO recipeDAO;
    Integer IngredientID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recette_activity_detail2);

        recyclerView = findViewById(R.id.IngredientRecyclerView);

        recipeDAO = AppDatabase.getDatabaseInstance(this).getRecipeDAO();

        Intent in = getIntent();
        Bundle b = in.getExtras();
        IngredientID = b.getInt("ingredient_recette");

        IngredientRecipeAdapter ingredientRecipeAdapter = new IngredientRecipeAdapter(recipeDAO.getRecetteByIngredient(IngredientID));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(ingredientRecipeAdapter);
    }
}
